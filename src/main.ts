import config from "config";
import i18n from "i18n";
import path from "path";
import { createLogger, transports } from "winston";

import { ISverkerConfig, Sverker } from "./sverker";

import IcebreakerCommand from "./commands/explicit/icebreaker.command";

i18n.configure({
  locales: ['en', 'de', 'sv'],
  directory: path.join(__dirname, '../locales'),
  defaultLocale: 'en',
  updateFiles: false,
  register: global,
});

const logger = createLogger({
  transports: [
    new transports.Console({
      level: config.get("logging.level"),
    }),
  ],
});

const sverkerConfig: ISverkerConfig = {
  logger,
  token: config.get("token"),
  youtubeChannels: config.get('channels.youtube'),
  twitchChannels: config.get('channels.twitch'),
  googleApiKey: config.get("googleApiKey"),
  twitchClientId: config.get("twitchClientId")
};

const sverker = new Sverker(sverkerConfig);

sverker.registerExplicitCommand(new IcebreakerCommand("icebreaker"));

sverker.start();

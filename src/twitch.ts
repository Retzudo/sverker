import config from "config";
import { Logger } from "winston";

const TWITCH_CLIENT_ID: string = config.get('twitchClientId');

export async function getTwitchFollowers(channelId: string, logger: Logger): Promise<number> {
  if (!TWITCH_CLIENT_ID) {
    return;
  }
  
  const url = `https://api.twitch.tv/kraken/channels/${channelId}/follows`;
  
  logger.debug('Twitch channel ID:', channelId);
  logger.debug('Twitch API URL:', url);

  const response = await fetch(url, {
    headers: {
      'Accept': 'application/vnd.twitchtv.v5+json',
      'Client-ID': TWITCH_CLIENT_ID
    }
  });
  logger.debug('Response:', response);
  
  if (response) {
    const json = await response.json();
    
    if (json) {
      return parseInt(json._total, 10);
    }
  }
}
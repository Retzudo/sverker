import Discord, { Guild, GuildMember, PartialGuildMember, TextChannel, User } from "discord.js";
import { Logger } from "winston";
import i18n from 'i18n';

import { ExplicitCommand, ICommand } from "./commands/command";
import { getTwitchFollowers } from "./twitch";
import { getYoutubeSubscribers } from "./youtube";

export interface ISverkerConfig {
  token: string;
  logger: Logger;
  youtubeChannels: {id: string, name: string}[],
  twitchChannels: {id: string, name: string}[],
  commandPrefix?: string;
  googleApiKey?: string;
  twitchClientId?: string;
}

export class Sverker {
  private client: Discord.Client;
  private config: ISverkerConfig;
  private implicitCommands: ICommand[] = [];
  private explicitCommands: ExplicitCommand[] = [];
  private lastYoutubeMilestones: {[name: string]: number} = {};
  
  constructor(config: ISverkerConfig) {
    this.config = config;
    
    if (!this.config.commandPrefix) {
      this.config.commandPrefix = "!";
    }
  }
  
  public start(): void {
    const { logger, token, googleApiKey, twitchClientId } = this.config;
    
    this.createClient();
    
    if (!token) {
      logger.error("Discord token not defined. Cannot start bot.");
      return;
    }
    
    if (!googleApiKey) {
      logger.warn("Google API key not defined. Certain commands will not work");
    }
    
    if (!twitchClientId) {
      logger.warn("Twitch client ID not defined. Certain commands will not work");
    }
    
    this.setUpEvents();
    
    this.client.login(this.config.token).catch(logger.error);
  }
  
  public registerImplicitCommand(command: ICommand): void {
    this.implicitCommands.push(command);
  }
  
  public registerExplicitCommand(command: ExplicitCommand): void {
    command.commandPrefix = this.config.commandPrefix;
    this.explicitCommands.push(command);
  }
  
  protected onMessage(message: Discord.Message): void {
    for (const command of this.implicitCommands) {
      command.execute(message);
    }
    
    for (const command of this.explicitCommands) {
      if (message.content.startsWith(this.config.commandPrefix)) {
        command.execute(message);
      }
    }
  }
  
  protected onGuildMemberRemove(guildMember: GuildMember | PartialGuildMember): void {
    const channels = this.client.channels.cache.filter(channel => channel.type === 'text' && ['moderators'].includes((channel as TextChannel).name));
    for (const channel of channels.values()) {
      (channel as TextChannel).send('**' + guildMember.displayName + '** stack härifrån. (<@!' + guildMember.id + '>)');
    }
  }
  
  protected onGuildBanAdd(guild: Guild, user: User):  void {
    const channels = this.client.channels.cache.filter(channel => channel.type === 'text' && ['moderators'].includes((channel as TextChannel).name));
    for (const channel of channels.values()) {
      (channel as TextChannel).send('<@!' + user.id + '> blev bannad åt helvete.');
    }
  }
  
  protected onReady(): void {
    const { client, config } = this;
    const { logger } = config;
    
    logger.info(`Logged in as ${client.user.username}#${client.user.discriminator} (${client.user.id})`);
    client.user.setActivity('sayitinswedish.com');
    
    const channels = client.channels.cache.filter(channel => channel.type === 'text' && ['general'].includes((channel as TextChannel).name));
    for (const channel of channels.values()) {
      const textChannel = channel as TextChannel;
      textChannel.send('Jag är vaken nu.');
      
      setTimeout(function() {
        textChannel.send('_Slaps Hapo with a large trout._');
      },1000*60*3);
    }
    
    this.setTopicContinuously();
  }
  
  protected setTopicContinuously(): void {
    this.setTopic();
    setInterval(() => {
      this.setTopic();
    }, 1000 * 60 * 15);
  }
  
  protected async setTopic(): Promise<void> {
    const channels = this.client.channels.cache.filter(channel => channel.type === 'text' && ['general'].includes((channel as TextChannel).name));
    const topicParts = [];
    const milestones = [];
    
    for (const channel of this.config.youtubeChannels) {
      const subscriberCount = await getYoutubeSubscribers(channel.id, this.config.logger);
      
      if (subscriberCount % 100 === 0) {
        if (subscriberCount > this.lastYoutubeMilestones[channel.name]) {
          milestones.push({
            channelName: channel.name,
            subscriberCount
          });
        }
        
        this.lastYoutubeMilestones[channel.name] = subscriberCount;
        this.config.logger.debug('lastYoutubeMilestones:', this.lastYoutubeMilestones[channel.name]);
        
      }
      
      topicParts.push(channel.name.toUpperCase() + ' YT: ' + subscriberCount);
    }
    
    for (const channel of this.config.twitchChannels) {
      const followerCount = await getTwitchFollowers(channel.id, this.config.logger);
      topicParts.push(channel.name.toUpperCase() + ' Twitch: ' + followerCount);
    }
    
    const theTopic = topicParts.filter(part => part).join(' | ');
    
    this.config.logger.debug('TOPIC:', theTopic);
    
    for (const channel of channels.values()) {
      const textChannel = channel as TextChannel;
      for (const milestone of milestones) {
        textChannel.send(i18n.__({
          phrase: 'youtube.milestone.' +  milestone.channelName,
          locale: 'sv'
        }, {
          subs: milestone.subscriberCount
        }));
      }
      
      textChannel.setTopic(theTopic);
    }
  }
  
  private createClient() {
    this.client = new Discord.Client();
  }
  
  private setUpEvents() {
    const { client, config } = this;
    const { logger } = config;
    
    client.once("ready", () => {
      logger.info(`Logged in as ${client.user.username}#${client.user.discriminator} (${client.user.id})`);
      this.onReady();
    });
    
    client.on("error", (error) => { logger.error(error); });
    client.on("warn", (warn) => { logger.warn(warn); });
    client.on("debug", (debug) => { logger.debug(debug); });
    
    client.on("message", (message) => { this.onMessage(message); });
    
    client.on('guildMemberRemove', (guildMember) => { this.onGuildMemberRemove(guildMember)});
    client.on('guildBanAdd', (guild, user) => { this.onGuildBanAdd(guild, user) });
  }
}

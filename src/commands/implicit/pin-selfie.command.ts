
import config from "config";
import { Message, TextChannel } from "discord.js";

import { ICommand } from "../command";

const SELFIE_CHANNEL_NAME = config.get("selfieChannelName");

export default class PinSelfieCommand implements ICommand {
  public execute(message: Message): void {
    const channel = message.channel as TextChannel;

    if (channel.name === SELFIE_CHANNEL_NAME) {
      const hasImage = message.attachments.some((attachment) => !!attachment.height);

      if (hasImage) {
        message.pin();
      }
    }
  }
}
import config from "config";
import { Message, TextChannel } from "discord.js";

import { ICommand } from "../command";

const SHARING_CHANNEL_NAME = config.get("sharingChannelName");

export default class RemoveTextInSharingChannelCommand implements ICommand {
  public execute(message: Message): void {
    const channel = message.channel as TextChannel;

    if (channel.name === SHARING_CHANNEL_NAME) {
      // There are unnecessary escape sequences in that regex but there's no way in hell
      // I'm going to touch it.
      // eslint-disable-next-line no-useless-escape
      const urlRegex = /((((http|https|ftp|sftp|rtmp):(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/i;

      if (message.attachments.size === 0 && message.content.match(urlRegex) === null) {
          message.delete();
      }
    }
  }
}

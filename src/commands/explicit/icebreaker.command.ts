import i18n from 'i18n';

import { Message } from "discord.js";
import { ExplicitCommand } from "../command";

export default class IcebreakerCommand extends ExplicitCommand {
  protected doExecute(message: Message): void {
    const icebreakerNumber = Math.floor(Math.random() * 373);
    const icebreaker = i18n.__({
      phrase: `icebreakers.${icebreakerNumber}`,
      locale: 'sv'
    });
    message.channel.send(icebreaker);
  }
}

import config from "config";
import { Message } from "discord.js";

const CASE_SENSITIVE_COMMANDS = config.get("caseSensitiveCommands");

export interface ICommand {
  execute(message: Message): void;
}

export abstract class ExplicitCommand implements ICommand {
  public commandPrefix: string;

  constructor(private trigger: string) { }

  public execute(message: Message): void {
    if (!this.isTriggered(message)) {
      return;
    }

    this.doExecute(message);
  }

  protected isTriggered(message: Message): boolean {
    const [firstWord, ..._] = message.content.split(" ", 1);
    const withoutPrefix = firstWord.substr(this.commandPrefix.length).trim();

    if (!CASE_SENSITIVE_COMMANDS) {
      return withoutPrefix.toLowerCase() === this.trigger.toLowerCase();
    } else {
      return withoutPrefix === this.trigger;
    }
  }

  protected abstract doExecute(message: Message): void;
}

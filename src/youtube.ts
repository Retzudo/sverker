import config from 'config';
import { Logger } from 'winston';

const GOOGLE_API_KEY: string = config.get('googleApiKey');

export async function getYoutubeSubscribers(channelId: string, logger: Logger): Promise<number> {
  if (!GOOGLE_API_KEY) {
    return;
  }
  
  const url = 'https://www.googleapis.com/youtube/v3/channels?part=statistics&id='
  + channelId
  + '&fields=items/statistics/subscriberCount&key='
  + GOOGLE_API_KEY;
  
  logger.debug('YouTube channel ID:', channelId);
  logger.debug('YouTube API URL:', url);
  
  const response = await fetch(url);
  logger.debug('Response:', response);
  
  if (response) {
    const json = await response.json();
    logger.debug('JSON:', JSON.stringify(json));
    
    if (json) {
      const { subscriberCount } = json.items[0].statistics;
      
      return parseInt(subscriberCount, 10);
    }
  }
}
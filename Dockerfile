FROM node:alpine

COPY ./dist/ /sverker
WORKDIR /sverker

ENV NODE_ENV=production

RUN npm install

CMD node ./dist/main.js